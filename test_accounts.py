import re
import logging

from database import *

log = logging.getLogger(__name__)


def reset_cube(cube):
    # try:
    print(f'{cube} Resetting user...')
    cube_id = cube.id
    AlgoAllocation.query.filter_by(cube_id=cube_id).delete()
    AssetAllocation.query.filter_by(cube_id=cube_id).delete()
    Balance.query.filter_by(cube_id=cube_id).delete()
    Connection.query.filter_by(cube_id=cube_id).delete()
    ConnectionError.query.filter_by(cube_id=cube_id).delete()
    CubeUserAction.query.filter_by(cube_id=cube_id).delete()
    CubeCache.query.filter_by(cube_id=cube_id).delete()
    DailyPerformance.query.filter_by(cube_id=cube_id).delete()
    ExternalTransaction.query.filter_by(cube_id=cube_id).delete()
    ExternalAddress.query.filter_by(cube_id=cube_id).delete()
    Order.query.filter_by(cube_id=cube_id).delete()
    Transaction.query.filter_by(cube_id=cube_id).delete()
    custom_ports = CustomPortfolio.query.filter_by(cube_id=cube_id).all()
    for custom in custom_ports:
        db_session.delete(custom)
    print(f'{cube} Reset')
    return True
    # except Exception as e:
    #     log.debug(e)
    #     return False


def delete_cube(cube):
    # try:
    print(f'{cube} Deleting cube...')
    if reset_cube(cube):
        db_session.delete(cube)
        db_session.commit()
        print(f'{cube} Deleted')
        return True
    # except Exception as e:
    #     log.debug(e)
    #     return False
   

def reset_user(user):
    try:
        log.debug(f'{user} Resetting user...')
        # Delete records
        cubes = Cube.query.filter_by(user_id=user.id).all()
        for cube in cubes:           
            delete_cube(cube)
        Cube.query.filter_by(user_id=user.id).delete()
        db_session.commit()

        user.portfolio = 0
        db_session.add(user)
        db_session.commit()
        log.debug(f'{user} Reset')
        return True
    except Exception as e:
        log.debug(e)
        return False


def delete_user(user):
    try:
        log.debug(f'{user} Deleting user...')
        user_id = user.id
        if reset_user(user):
            ConnectionError.query.filter_by(user_id=user.id).delete()
            DailyPerformanceAccount.query.filter_by(user_id=user_id).delete()
            Ledger.query.filter_by(user_id=user.id).delete()
            UserApiKey.query.filter_by(user_id=user.id).delete()
            UserNotification.query.filter_by(user_id=user.id).delete()
            UserPoint.query.filter_by(user_id=user.id).delete()
            UserProduct.query.filter_by(user_id=user.id).delete()
            User.query.filter_by(id=user.id).delete()
            db_session.commit()
            log.debug(f'{user_id} Deleted')
            return True
    except Exception as e:
        log.debug(e)
        return False


def clear_test_accounts():
    users = User.query.all()
    for user in users:
        if re.search('(^test-e2e-account-).+(@coincube.io)', user.email):
            delete_user(user)
        if user.email in ['test_basic@test.com', 'test_pro@test.com']:
            reset_user(user)
