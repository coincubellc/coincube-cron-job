import logging
import random
import string
from collections import OrderedDict, defaultdict
from enum import Enum, auto
from typing import List, Set
import pandas as pd

from bt_utils import load_OHLC_data, load_mcap_data
from database import BacktestResults, engine, db_session, BacktestResultsSecondary

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class InsufficientDataException(Exception):
    """ Raised when index construction requires larger number
    of currencies than available in the data for that day.
    """
    pass


class RebalancePeriod(Enum):
    daily = auto()
    weekly = auto()
    monthly = auto()


class RebalanceType(Enum):
    EqualyWeighted = auto()
    MCapWeighted = auto()


def all_symbol_selector(symbols, *args):
    return symbols

def mcap_selector(symbols, date, mcap, index_size=5, data_bounds=None,
                  adaptive_index=False):
    """ Select the top 'index_size' symbols that have the largest market cap
    on the provided date. If 'adaptive_index' is True, the index will be built
    even if there are fewer symbols available that the index requires.
    """

    data = {sym: ser[date] for sym, ser in mcap.items()
            if date in ser.index}
    data = pd.Series(data)
    data = data.sort_values(ascending=False)

    selection = data.index
    selection = [s for s in selection if s in symbols]
    oob_symbols = [s for s, b in data_bounds.items()
                   if date < b[0] or date > b[1]]

    selection = [s for s in selection if s not in oob_symbols]
    if len(selection) < index_size:
        if adaptive_index:
            s = f"Data for only {len(selection)} symbols available on {date}. Reducing index size (from {index_size})."
            log.info(s)
        else:
            raise InsufficientDataException(f"Data for only {len(selection)} symbols available for date {date}. Index size: {index_size}")

    selection = selection[:index_size]
    assert len(selection) == len(set(selection))

    return selection


def get_rebalance_dates(dates, period: RebalancePeriod) -> Set:
    if period is RebalancePeriod.monthly:
        return {d for d in dates if d.day == 1}
    elif period is RebalancePeriod.weekly:
        return {d for d in dates if d.weekday() == 0}
    else:  # if period is RebalancePeriod.daily:
        return set(dates)


def rebalance_EW(dt, mon, prices_current, symbol_list, *args):
    alloc_part = mon / len(symbol_list)

    cur_positions = {}
    for sym in symbol_list:
        cur_positions[sym] = alloc_part / prices_current[sym]

    return cur_positions


def rebalance_MW(dt, mon, prices_current, symbol_list, mcap_data):
    """ Rebalance asset allocations using asset MarketCap.

    Each symbol's percentage in the portfolio is proportional to
    it's market capitalization.
    """
    mcap = {sym: mcap_data[sym].loc[dt] for sym in symbol_list}
    mcap_sum = sum(mcap.values())

    allocs = {sym: mon * (mcap[sym] / mcap_sum)
              for sym in symbol_list}

    cur_positions = {sym: allocs[sym] / prices_current[sym]
                     for sym in symbol_list}

    return cur_positions


def apply_rebal_threshold(cur_positions, new_positions, rebalance_threshold):
    """ Check if the distance from current allocation to the desired
     allocations according to the rebalancing algorithm. If the difference is greater
     than the `rebalance_threshold` percent, rebalance that symbol.
    """
    if rebalance_threshold is None or cur_positions is None:
        return new_positions
    else:
        # if the symbol is added in the new rebalancing, use the new allocation
        result = {sym: pos for sym, pos in new_positions.items()
                  if sym not in cur_positions}

        syms = set(cur_positions.keys()).intersection(new_positions.keys())
        for sym in syms:
            pct_dif = (cur_positions[sym] - new_positions[sym]) / cur_positions[sym]
            if abs(pct_dif) < rebalance_threshold:
                # keep current allocs
                result[sym] = cur_positions[sym]
            else:
                # rebalance
                result[sym] = new_positions[sym]

        return result


def backtest(universe: List[str], start_date, end_date, initial_capital, backtest_name=None, save_to_db=True, data=None,
             mcap_data=None, rebalance_period=RebalancePeriod.monthly, rebalance_type=RebalanceType.EqualyWeighted,
             index_name="", symbol_selector=all_symbol_selector, rebalance_threshold=None,
             adaptive_index_construction=True, same_day_entry=True):
    """ Run the backtest for index rebalancing.

    OHLC / MCap data can be provided with `data` and `mcap_data` parameters,
    so that the data doesn't have to be fetched for each backtest.
    """

    log.info(f"Starting backtest {backtest_name}.")
    log.debug(f'Backtest parameters: start_date={start_date}, end_date={end_date}, initial_capital={initial_capital}, '
              f'save_to_db={save_to_db}, rebalance_period={rebalance_period}, rebalance_type={rebalance_type}, '
              f'index_name={index_name}, rebalance_threshold={rebalance_threshold}, adaptive_index={adaptive_index_construction}, '
              f'same_day_entry={same_day_entry}')

    if backtest_name is not None and save_to_db:
        if len(backtest_name) > BacktestResultsSecondary.MAX_NAME_LENGTH:
            log.error(f"Maximum name length is {BacktestResultsSecondary.MAX_NAME_LENGTH}. Provided name: {backtest_name}.")
            return None

        # assert that the name is not in use
        names ={n[0] for n in db_session.query(
            BacktestResultsSecondary.backtest_name).distinct()}

        if backtest_name in names:
            log.error("Backtest name already in use!")
            return None
    else:
        backtest_name = generate_name()

    # select weighting function
    if rebalance_type is RebalanceType.EqualyWeighted:
        rebalance = rebalance_EW
    else:
        rebalance = rebalance_MW

    if data is None:
        data = load_OHLC_data(universe, start_date, end_date)

    if mcap_data is None:
        mcap_data = load_mcap_data(universe, start_date, end_date)

    # skip the symbols that don't have the required data
    skip_list = {s for s in universe if s not in data}
    skip_list |= {sym for sym, df in data.items()
                  if df is None or not len(df)}
    skip_list = sorted(skip_list)
    if skip_list:
        log.info(f"Data loading failed for symbols {skip_list}. ")

    # find the date range for each symbol where both OHLC and MCap data are available
    data_bounds = {}
    for sym in data:
        if sym in skip_list:
            continue

        if sym not in mcap_data:
            log.info(f"No MCap data for symbol {sym}. Skipping.")
            skip_list.append(sym)
            continue

        m = mcap_data[sym]
        if m.index[0] < data[sym].index[0]:
            print(f"Earlier mcap data start for {sym}. {m.index[0]} mcap, {data[sym].index[0]} ohlc.")

        start = max(m.index[0], data[sym].index[0])

        if m.index[-1] > data[sym].index[-1]:
            print(f"Later mcap data end for {sym}. {m.index[-1]} mcap, {data[sym].index[-1]} ohlc.")
        end = min(m.index[-1], data[sym].index[-1])
        data_bounds[sym] = (start, end)

    universe = [s for s in universe if s not in skip_list]
    data = {sym: data[sym] for sym in universe
            if sym in data}


    start_date, end_date = get_start_end(data, start_date, end_date)

    data = {sym: df for sym, df in data.items()}
    for sym in universe:
        if sym not in data:
            continue

        df = data[sym]
        df = df.truncate(before=start_date, after=end_date)
        data[sym] = df

    idx = pd.date_range(start_date, end_date)
    rebalance_dates = get_rebalance_dates(idx, rebalance_period)
    cash = 0
    cur_positions = None
    positions = OrderedDict()
    pfolio_val = OrderedDict()
    result = defaultdict(list)
    selection = None

    # main backtest loop
    for i, dt in enumerate(idx):
        if i == 0:
            selection = symbol_selector(universe, dt, mcap_data,
                                        data_bounds=data_bounds,
                                        adaptive_index=adaptive_index_construction)
        prices_current = get_prices(selection, dt, data)

        # get current portfolio value
        if cur_positions is None:
            mon = initial_capital
        else:
            mon = 0
            for sym, m in cur_positions.items():
                # handle sym no longer in prices
                if sym not in prices_current:
                    log.warning(f"Symbol {sym} in index, but not in data.")
                    # find last available data
                    p = data[sym].truncate(after=dt).close[-1]
                else:
                    p = prices_current[sym]

                mon += p * m

        # update results
        result['positions'] += [cur_positions]
        result['value'] += [mon]
        result['cash'] += [cash]

        # check if should rebalance
        if dt not in rebalance_dates:
            continue

        if i == len(idx) - 1:
            continue

        ### rebalance
        log.debug(f"\n Rebalance @: {dt} {'%' * 20}")
        old_sel = selection
        selection = symbol_selector(universe, dt, mcap_data,
                                    data_bounds=data_bounds,
                                    adaptive_index=adaptive_index_construction)
        prices_current = get_prices(selection, dt, data)
        if old_sel is not None:
            log.debug(f"New symbols: {set(selection).difference(old_sel)}")

        if same_day_entry:
            entry_price = get_prices(selection, dt, data)
        else:
            dt_pos = idx.get_loc(dt)
            next_dt = idx[dt_pos + 1]
            entry_price = get_prices(selection, next_dt, data)

        log.debug(f"Prices: {prices_current}")
        log.debug(f"Current positions: {cur_positions}")

        log.debug(f"Value: {mon}, Cash: {cash}, Value + cash: {mon + cash}")
        mon += cash
        pfolio_val[dt] = mon

        new_positions = rebalance(dt, mon, prices_current, selection, mcap_data)
        cur_positions = apply_rebal_threshold(cur_positions, new_positions,
                                              rebalance_threshold)

        entry_price = sum(entry_price[sym] * cur_positions[sym]
                          for sym in selection)
        cash = mon - entry_price

        log.debug(f"New positions: {cur_positions}")
        positions[dt] = cur_positions

        log.debug('')

    result = pd.DataFrame(result, index=idx)
    result.index.name = 'timestamp'

    # write the backtest results to the database
    if save_to_db:
        log.debug("Saving backtests result to the database.")
        db_results = result.reset_index()
        db_results['backtest_name'] = backtest_name
        db_results['index_name'] = index_name
        db_results['portfolio_value'] = db_results['value'] + db_results['cash']
        db_results = db_results[['timestamp', 'backtest_name', 'portfolio_value', 'index_name']]
        db_results.to_sql(BacktestResultsSecondary.__tablename__, engine,
                          index=False, if_exists='append', chunksize=10000)
    else:
        log.debug("Skipping saving backtest results to the database.")

    log.info(f"Finished backtest {backtest_name}.")
    return result


def get_prices(universe, dt, data):
    prices = {}
    for sym in universe:
        df = data[sym]
        if dt in df.index:
            p = df.loc[dt, 'close']
        else:
            df = df.truncate(after=dt)
            p = df.iloc[-1].close

        prices[sym] = p

    return prices


def get_start_end(data, start_date, end_date):
    if start_date is not None:
        start_date = start_date.replace(tzinfo=None)

    if end_date is not None:
        end_date = end_date.replace(tzinfo=None)

    data_beginnings = [df.index[0] for df in data.values()]
    data_start = min(data_beginnings)

    if start_date is None:
        start_date = data_start
    else:
        if start_date < data_start:
            start_date = data_start
            log.info(f"Increased start date to {start_date} in order match the earliest start date.")

    # do the same for data ends
    data_endings = [df.index[-1] for df in data.values()]
    data_end = max(data_endings)
    if end_date is None:
        end_date = data_end
    else:
        if end_date > data_end:
            end_date = data_end
            log.info(f"Decreased the backtest end date to {end_date} in order to match the last end date.")

    return start_date, end_date


def generate_name():
    symbols = string.digits + string.ascii_uppercase
    name = [random.choice(symbols) for _ in range(BacktestResults.MAX_NAME_LENGTH)]
    name = ''.join(name)
    return name


