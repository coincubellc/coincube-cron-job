#!/usr/bin/env python3
from time import sleep
import logging
import requests


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

_icons_url = 'https://s2.coinmarketcap.com/static/img/coins/32x32/'
_base_url = 'https://pro-api.coinmarketcap.com'
headers = {'X-CMC_PRO_API_KEY': '932e9896-d43d-4f5e-87e1-ac4d47ba8df6'}

# Get market data from CMC
log.info("Getting tickers from CoinMarketCap...")

def get_markets():
    # Get market data from CMC
    log.info("Getting markets from CoinMarketCap...")
    url = _base_url + '/v1/cryptocurrency/listings/latest?start=1&limit=500'
    resp = requests.get(url, headers=headers)
    if resp.status_code == 200:
        markets = resp.json()['data']
        return markets
    else:
        return []

def get_icons():
    markets = get_markets()

    for market in markets:
        cur_id = market['id']
        symbol = market['symbol']

        try:
            # Add icon
            url = _icons_url + f'{cur_id}.png'
            resp = requests.get(url, allow_redirects=True)
            open(f'icons/{symbol}.png', 'wb').write(resp.content)
            log.debug(f'Added icon for: {symbol}')
            sleep(1)
        except:
            log.debug(f'Unable to add icon for: {symbol}')
