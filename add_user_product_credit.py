from database import *

user_products = UserProduct.query.all()

for up in user_products:
    # Add additional time to all accounts
    up.paid_until += timedelta(days=33)
    print(up.paid_until)
    db_session.add(up)

db_session.commit()