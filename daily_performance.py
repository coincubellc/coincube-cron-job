#!/usr/bin/env python3
import logging
from datetime import datetime, timedelta
import pandas as pd

from database import (Cube, DailyPerformanceAccount, 
                      db_session, User, get_prices)

log = logging.getLogger(__name__)


def update_cube(cube, prices, timeframe):
    cube.performance_gen(prices, timeframe=timeframe)
    cube.update_charts = 0
    cube.user.update_at = datetime.utcnow()
    cube.save_to_db()  

def performance_cube(cube_id, timeframe='daily'):
    log.debug(f'Updating {timeframe} Performance for Cube {cube_id}')
    cube = Cube.query.filter_by(id=cube_id).first()
    frequency = 'H'
    if timeframe == 'daily':
        frequency = 'D'
    prices = get_prices(frequency)
    log.debug(f'{cube} Updating...')
    try:
        log.debug(f'{cube} Regular run')
        update_cube(cube, prices, timeframe)
    except Exception as e:
        log.exception(f'[Cube {cube_id}] Exception while processing')
    log.debug('Finished Daily Performance Update')


def performance_cubes(timeframe='hourly'):
    log.debug(f'Updating {timeframe} Performance for all Cubes')
    frequency = 'H'
    if timeframe == 'daily':
        frequency = 'D'
    prices = get_prices(frequency)
    cubes = Cube.query.all()
    for cube in cubes:
        if cube.api_connections:
            log.debug(f'{cube} Updating...')
            try:
                log.debug(f'{cube} Regular run')
                update_cube(cube, prices, timeframe)
            except Exception as e:
                log.exception('[Cube %d] Exception while processing' % cube.id)
    log.debug('Finished Daily Performance Update')

def performance_account(timeframe='hourly'):
    log.debug('Updating Daily Performance for all Accounts')

    users = User.query.all()

    for user in users:
        if user.cubes:
            try:
                log.debug(f'{user} Updating...')
                if user.updated_at + timedelta(hours=1) < datetime.utcnow():
                    if timeframe == 'daily':
                        user.daily_performance_gen()
                        user.updated_at = datetime.utcnow()
                        user.save_to_db()
                    if timeframe == 'hourly':
                        user.hourly_performance_gen()
                        user.updated_at = datetime.utcnow()
                        user.save_to_db()
            except Exception as e:
                log.exception('[User %d] Exception while processing' % user.id)
    log.debug('Finished Daily Performance Account Update')

if __name__ == '__main__':
    performance_cubes(timeframe='daily')
    performance_account(timeframe='daily')