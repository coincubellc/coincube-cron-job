from database import (db_session, ExPair, IndexPair, Cube)


def add_cube_fiat_id():
    cubes = Cube.query.all()

    for cube in cubes:
        ex_pair = ExPair.query.filter_by(
            exchange_id=cube.exchange.id,
            active=True).filter(
            ExPair.base_symbol == 'BTC').first()
        if not ex_pair:
            continue
        # Use first ex_pair with fiat quote
        cube.fiat_id = ex_pair.quote_currency_id
        print(cube)
        print(cube.fiat.symbol)
        db_session.add(cube)
    db_session.commit()

if __name__ == '__main__':
    add_cube_fiat_id()