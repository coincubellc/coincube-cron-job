#!/usr/bin/env python3
import os
import requests
from database import *


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

BASE_URL = os.getenv('EXAPI_URL')


def get_details(ex_pair):
    url = f'{BASE_URL}/{ex_pair.exchange.name}/details'
    params = {
    	'base': ex_pair.base_currency.symbol,
    	'quote': ex_pair.quote_currency.symbol,
    }
    response = requests.get(url, params=params)
    response.raise_for_status()
    json_content = response.json()
    return json_content

def update_ex_pair_limits():
    ex_pairs = ExPair.query.filter_by(active=True).all()

    for ex_pair in ex_pairs:

        if ex_pair.exchange.name in ['External', 'Manual']:
            continue

        log.debug(f'Get details for {ex_pair}')
        try:
            details = get_details(ex_pair)
            epl = ExPairLimits.query.filter_by(ex_pair_id=ex_pair.id).first()
            if epl:
                log.debug('Updating details')
                epl.ex_pair_id = ex_pair.id
                epl.min_amount = details['min_amt']
                epl.min_value = details['min_val']
                epl.save_to_db()
            else:
                log.debug('Adding details')
                epl = ExPairLimits(
                        ex_pair_id=ex_pair.id,
                        min_amount=details['min_amt'],
                        min_value=details['min_val'],
                        )
                epl.save_to_db()
        except:
            log.debug(f'Error adding {ex_pair} limits')

if __name__ == '__main__':
    update_ex_pair_limits()