import logging
import os

import pandas as pd

from bt_utils import load_currency, get_all_currencies

# ###############################
# Run this to cache the OHLC data
##################################


USE_CACHE = False  # other modules use of cached data disable here

DATA_DIR = '/var/tmp/data_tmp/'

# the list of symbols to be cached
# only currencies with pairs that are quoted in BTC
# are supported ATM (so no BTC, for example)
currencies = get_all_currencies()

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def preload():
    """ Retrieve hourly data from the database and cache it locally to HDF5.
    """
    global DATA_DIR
    DATA_DIR = os.path.abspath(DATA_DIR)
    if not os.path.exists(DATA_DIR):
        log.info(f"Directory {DATA_DIR} doesn't exits. Creating.")
        os.mkdir(DATA_DIR)

    for c in currencies:
        log.info("Preloading data for {}".format(c.symbol))
        data = load_currency(c, None, None, None,
                             resample=False, use_usd=False)

        if data is None:
            log.error("Couldn't get the data for {} from the database. Skipping caching".format(c.symbol))
            continue

        id = 'C' + str(c.id)
        dest = os.path.join(DATA_DIR, id + '.h5')
        if os.path.exists(dest):
            log.info("File exists. Overwriting.")

        data.to_hdf(dest, id, mode='w')


def load_from_cache(cur_id):
    cur_id = "C" + str(cur_id)
    sym_path = os.path.join(DATA_DIR, cur_id + '.h5')
    if os.path.exists(sym_path):
        return pd.read_hdf(sym_path, cur_id)


if __name__ == '__main__':
    log = logging.getLogger()
    log.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    cf = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s',
                           datefmt='%H:%M:%S')
    ch.setFormatter(cf)
    log.addHandler(ch)

    preload()
