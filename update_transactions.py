from database import *
from datetime import datetime, timedelta
import numpy as np
import requests
from time import time

log = logging.getLogger(__name__)

_exapi_url = os.getenv('EXAPI_URL')


def get_api_creds(cube, exchange):
    conn = Connection.query.filter_by(cube_id=cube.id, exchange_id=exchange.id).first()
    if conn.exchange_id == 12: # External or Manual
        creds = {
            'key': conn.key,
            'secret': conn.secret,
            'passphrase': conn.passphrase            
        }
    else:
        # API credentials
        creds = {
            'key': conn.decrypted_key,
            'secret': conn.decrypted_secret,
            'passphrase': conn.decrypted_passphrase
        }
    return creds

def api_request(request_type, exchange, endpoint, params):
    url = f'{_exapi_url}/{exchange}{endpoint}'
    r = requests.request(request_type, url, params=params)
    log.debug(r.status_code)
    if r.status_code == 200:
        json_content = r.json()
        return json_content
    if r.status_code == 400:
        return 'InvalidOrder'
    if r.status_code == 503:
        return None 
    if r.status_code == 401:
        return None    
    if r.status_code == 403:
        return None                 


def get_start_timestamp(cube, db_tx):
    if db_tx:
        log.debug(f'{cube} Tranasactions exist, updating from {db_tx.timestamp}')
        return int(float(db_tx.timestamp)) + 1
    else:
        log.debug(f'{cube} Transactions do not exist, updating from Jan 1, 2014')
        return 1325376000000 # Jan 1, 2012


def import_trades(cube, ex, creds, since):
    log.debug(f'{cube} Importing Trades')
    now = time() * 1000
    days = 10
    trades = pd.DataFrame()
    url = '/trades'

    if ex.name == 'Binance':
        while since < now:
            new_trades = pd.DataFrame()
            for bal in cube.balances:
                ex_pairs = ExPair.query.filter_by(
                            exchange_id=ex.id, active=True
                            ).filter(or_(
                                ExPair.base_currency_id == bal.currency_id,
                                ExPair.quote_currency_id == bal.currency_id,
                            )).all()
                for ex_pair in ex_pairs:
                    args = {**creds,
                        **{
                            'base': ex_pair.base_symbol,
                            'quote': ex_pair.quote_symbol,
                            'limit': 1000,
                            'since': since
                        }
                    }
                    binance_trades = api_request('GET', ex.name, url, args)
                    binance_trades = pd.read_json(binance_trades)
                    new_trades = new_trades.append(binance_trades)
            if not new_trades.empty:
                new_trades = new_trades.sort_index()
                new_trades.timestamp = new_trades.timestamp.astype(np.int64)//10**6
                since = new_trades.iloc[-1].timestamp + 1
                trades = trades.append(new_trades)
            elif since < now:
                since = since + 24 * 60 * 60 * days * 1000
            else:
                break

    else:
        while since < now:
            args = {**creds, **{'since': since}}
            new_trades = api_request('GET', ex.name, url, args)
            if not new_trades:
                break
            new_trades = pd.read_json(new_trades)
            if not new_trades.empty:
                new_trades.timestamp = new_trades.timestamp.astype(np.int64)//10**6
                since = new_trades.iloc[-1].timestamp + 1
                trades = trades.append(new_trades)
            elif since < now:
                since = since + 24 * 60 * 60 * days * 1000
            else:
                break

    if not trades.empty:
        # Adjustments to dataframe to match table structure
        fee = trades['fee'].apply(pd.Series)
        try:
            fee = fee.drop(['type'], axis=1)
        except:
            pass
        try:
            fee = fee.rename(index=str, columns={'rate': 'fee_rate', 'cost': 'fee_amount', 'currency': 'fee_currency'})
        except:
            pass
        trades = pd.concat([trades, fee], axis=1)
        trades = trades.rename(index=str, columns={'id': 'tx_id', 'order': 'order_id', 'amount': 'base_amount', 'cost': 'quote_amount'})
        symbol = trades['symbol'].str.split('/', n=1, expand=True)
        trades['base_symbol'] = symbol[0]
        trades['quote_symbol'] = symbol[1]
        trades['trade_type'] = trades['type']
        trades['type'] = trades['side']
        trades.drop(['side', 'symbol', 'fee'], axis=1, inplace=True)
        _, i = np.unique(trades.columns, return_index=True)
        trades = trades.iloc[:, i]
        trades = trades.fillna(value=0)

        # Add trades to database
        log.debug(f'{cube} Writing trades to database')
        for index, row in trades.iterrows():
            ex_pair = ExPair.query.filter_by(
                        exchange_id=ex.id,
                        quote_symbol=row.quote_symbol,
                        base_symbol=row.base_symbol,
                        ).first()
            if not ex_pair:
                continue
            if not row.base_amount:
                continue
            trade = TransactionFull(
                    tx_id=row.tx_id,
                    datetime=index,
                    order_id=row.order_id,
                    type=row.type,
                    trade_type=row.trade_type,
                    price=row.price,
                    base_symbol=row.base_symbol,
                    quote_symbol=row.quote_symbol,
                    exchange=ex,
                    cube=cube,
                    user=cube.user,
                )
            db_session.add(trade)
            db_session.commit()
            db_session.refresh(trade)
            if row.type == 'buy':
                trade.base_amount = row.base_amount
                trade.quote_amount = -row.quote_amount
            elif row.type == 'sell':
                trade.base_amount = -row.base_amount
                trade.quote_amount = row.quote_amount
            try:
                trade.fee_rate = row.fee_rate
                trade.fee_amount = row.fee_amount
                trade.fee_currency = row.fee_currency
            except:
                pass
            print(trade)
            db_session.add(trade)
            db_session.commit()


def import_transactions(cube, ex, creds, since):
    print(f'{cube} Importing Transactions')
    trans = pd.DataFrame()
    url = '/transactions'

    while True:
        args = {**creds, **{'since': since}}
        new_trans = api_request('GET', ex.name, url, args)
        if not new_trans:
            break
        new_trans = pd.read_json(new_trans)
        print(new_trans)
        if not new_trans.empty:
            new_trans.timestamp = new_trans.timestamp.astype(np.int64)//10**6
            since = new_trans.iloc[-1].timestamp + 1
            trans = trans.append(new_trans)
        else:
            break

    if not trans.empty:
        print(f'{cube} Adjusting dataframe to match table structure')
        # Adjustments to dataframe to match table structure
        if trans['fee'].any():
            fee = trans['fee'].apply(pd.Series)
            try:
                fee = fee.rename(index=str, columns={'rate': 'fee_rate', 'cost': 'fee_amount'})
                trans = pd.concat([trans, fee], axis=1)
            except:
                log.debug(f'{cube} missing transaction fee information, skipping...')
            trans.drop(['fee'], axis=1, inplace=True)
        trans = trans.rename(index=str, columns={'id': 'tx_id', 'txid': 'order_id'})
        trans.drop(['status', 'updated', 'timestamp'], axis=1, inplace=True)
        _, i = np.unique(trans.columns, return_index=True)
        trans = trans.iloc[:, i]
        trans = trans.fillna(value=0)

        # Add transactions to database
        print(f'{cube} Writing transactions to database')
        for index, row in trans.iterrows():
            log.debug(row)
            log.debug(row.currency)
            if not row.amount:
                continue
            cur = Currency.query.filter_by(symbol=row.currency).first()
            if not cur:
                continue
            if row.type not in ['deposit', 'withdrawal', 'withdraw']:
                continue
            if row.type == 'withdraw':
                t_type = 'withdrawal'
            else:
                t_type = row.type

            ex_pair = ExPair.query.filter_by(
                        exchange_id=ex.id,
                        base_currency_id=cur.id,
                        ).first()
            if t_type == 'deposit':
                base_amount = row.amount
            elif t_type == 'withdrawal':
                base_amount = -row.amount
            quote_amount = 0
            if not ex_pair:
                ex_pair = ExPair.query.filter_by(
                        exchange_id=ex.id,
                        quote_currency_id=cur.id,
                        ).first()
                base_amount = 0
                if t_type == 'deposit':
                    quote_amount = row.amount
                elif t_type == 'withdrawal':
                    quote_amount = -row.amount

            tx = TransactionFull(
                    tx_id=row.tx_id,
                    datetime=index,
                    order_id=row.order_id,
                    tag=row.tag,
                    type=t_type,
                    base_symbol=ex_pair.base_currency.symbol,
                    quote_symbol=ex_pair.quote_currency.symbol,
                    exchange=ex,
                    cube=cube,
                    user=cube.user,
                )
            print(tx)
            print(base_amount)
            print(quote_amount)
            db_session.add(tx)
            db_session.commit()
            db_session.refresh(tx)
            tx.base_amount = base_amount
            tx.quote_amount = quote_amount
            db_session.add(tx)
            db_session.commit()


def update_transactions(cube, creds):
    log.debug(f'{cube} Updating transactions')
    print('updating transactions')
    db_tx = TransactionFull.query.filter_by(
                    cube_id=cube.id
                    ).order_by(
                    TransactionFull.id.desc()
                    ).first()

    # try:
    ts = get_start_timestamp(cube, db_tx)
    print(ts)
    import_trades(cube, cube.exchange, creds, ts)
    import_transactions(cube, cube.exchange, creds, ts)
    # except Exception as e:
    #     log.debug(e)
    #     return False
    #
    # return True


def run_test(cube_id=None):
    print('Running Test')
    cube = Cube.query.filter_by(id=cube_id).first()
    print(cube)
    creds = get_api_creds(cube, cube.exchange)
    print(creds)
    update_transactions(cube, creds)


def update_all_transactions():
    cubes = Cube.query.all()
    now = datetime.utcnow()
    for cube in cubes:
        try:
            log.debug(cube)
            last_tx = cube.transactions_full.order_by(TransactionFull.datetime.desc()).first()
            log.debug(last_tx)
            if cube.exchange_id in [12, 13]:
                if last_tx and last_tx.datetime + timedelta(hours=6) < now:
                    log.debug(f'{cube} More than 6 hours since last transaction')
                    creds = get_api_creds(cube, cube.exchange)
                    update_transactions(cube, creds)
                else:
                    log.debug(f'{cube} No transactions, first run')
                    creds = get_api_creds(cube, cube.exchange)
                    update_transactions(cube, creds)
            elif cube.exchange_id == 11:
                if last_tx and last_tx.datetime + timedelta(hours=3) < now:
                    log.debug(f'{cube} More than 3 hours since last transaction')
                    creds = get_api_creds(cube, cube.exchange)
                    update_transactions(cube, creds)
                else:
                    log.debug(f'{cube} No transactions, first run')
                    creds = get_api_creds(cube, cube.exchange)
                    update_transactions(cube, creds)
            else:
                if last_tx and last_tx.datetime + timedelta(hours=1) < now:
                    log.debug(f'{cube} More than 1 hour since last transaction')
                    creds = get_api_creds(cube, cube.exchange)
                    update_transactions(cube, creds)
                else:
                    log.debug(f'{cube} No transactions, first run')
                    creds = get_api_creds(cube, cube.exchange)
                    update_transactions(cube, creds)
        except Exception as e:
            log.debug(e)


if __name__ == '__main__':
    # update_all_transactions()
    run_test(cube_id=2710)