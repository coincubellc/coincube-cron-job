import calendar
import datetime
from collections import Counter
from typing import Dict

import numpy as np
import pandas as pd

from common import read_df, db_session
from database import ExPair, Candles1hLocal


target_table = Candles1hLocal


def table_stats(table):
    """ Calculates stats for the table.
    """
    print("Calculating table stats. This may take a few minutes.")
    pair_ids = db_session.query(table.ex_pair_id).distinct().all()
    pair_ids = [p[0] for p in pair_ids]
    pairs = ExPair.query.filter(ExPair.id.in_(pair_ids)).all()
    # collect individual stats
    pair_stats = [calc_pair_stats(p, table) for p in pairs]

    stats = {
        'n_pairs': len(pair_stats),
        'total_candles': sum(len(s['total']) for s in pair_stats),
        'nulls': sum(s['nulls'] for s in pair_stats),
        'infs': sum(s['infs'] for s in pair_stats),
        'zero_or_less': sum(s['zeros'] for s in pair_stats),
        'err_high': sum(s['err_high'] for s in pair_stats),
        'err_low': sum(s['err_low'] for s in pair_stats),
    }

    print("Pair stats: ")
    for k, v in stats.items():
        print(k, v)

    print()
    stats.update(update_stats(pair_stats))


def update_stats(pair_stats) -> Dict:
    """ Stats about pair updates: number of current symbols, delays, etc.
    """
    def get_timestamp(dt):
        return calendar.timegm(dt.utctimetuple())

    newest_ts = datetime.datetime.utcnow()
    newest_ts = newest_ts.replace(minute=0, second=0, microsecond=0)
    newest_ts -= datetime.timedelta(seconds=3600)
    newest_ts = get_timestamp(newest_ts)

    delays = (int((newest_ts - get_timestamp(p['end'])) / 3600)
              for p in pair_stats)
    counts = Counter(delays)

    stats = {
        'partial': counts[-1],
        'current': counts[0],
        'current -1': counts[1],
        'current -2': counts[2],
        'most_freq_delay': counts.most_common(1)[0],
    }
    print("Update stats:")
    print(f"Partial candles (should be 0): {stats['partial']}")
    print(f'Up-to-date pairs: {stats["current"]}')
    print(f'Pairs with delay 1: {stats["current -1"]}')
    print(f'Pairs with delay 2: {stats["current -2"]}')
    m = stats['most_freq_delay']
    print(f'Most frequent delay: {m[0]} hours ({m[1]} pairs total)')

    return stats


def calc_pair_stats(pair, table):
    """ Stats for a single pair.
    """
    df = read_df(table, pair)
    diff, gaps = find_gaps(df)
    candle_errors = find_candle_errors(df)

    stats = {'name': str(pair),
             'start': df.index[0],
             'end': df.index[-1],
             'total': df,
             'duplicates': len(df) - len(df.index.unique()),
             'missing': diff,
             'gaps': gaps,
             'nulls': candle_errors['null'].sum(),
             'infs': candle_errors['inf'].sum(),
             'zeros': candle_errors['zero_or_less'].sum(),
             'err_high': candle_errors['err_high'].sum(),
             'err_low': candle_errors['err_low'].sum(),
             }

    return stats


def find_candle_errors(df) -> pd.DataFrame:
    """ Finds different types of candle errors: infs, nans, zero values, ...

    'any' column of the returned dataframe is a union of all error types. Each
    individual error type has it's column as well.
    """
    err_high = df.high < df.loc[:, ['open', 'close', 'low']].max(axis=1)
    err_low = df.low > df.loc[:, ['open', 'high', 'close']].min(axis=1)
    zero_or_less = (df.loc[:, ['open', 'high', 'low', 'close']] <= 0).any(axis=1)

    nulls = pd.isnull(df).any(axis=1)
    infs = np.isinf(df).any(axis=1)

    any_ = err_high | err_low | zero_or_less | nulls | infs

    # just_none = lambda x: None if not x.any() else x

    stats = dict(
        err_high=err_high,
        err_low=err_low,
        zero_or_less=zero_or_less,
        null=nulls,
        inf=infs,
        any=any_
    )

    stats = pd.DataFrame(stats, index=df.index)
    return stats


def find_gaps(df: pd.DataFrame):
    """ Finds gaps in the dataframe index (missing timestamps).
    """
    # create the true ts index
    start_dt = df.index[0]
    end_dt = df.index[-1]

    rng = pd.date_range(start=start_dt, end=end_dt, freq='1H')
    diff = rng.difference(df.index)

    if not len(diff):
        return None, None

    # find gaps
    start = diff[0]
    gaps = []

    for i in range(1, len(diff)):
        if (diff[i] - diff[i - 1]).seconds > 3600:
            gaps.append((start, diff[i - 1]))
            start = diff[i]

    gaps.append((start, diff[-1]))
    return diff, gaps


if __name__ == '__main__':
    table_stats(target_table)
