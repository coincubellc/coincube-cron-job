#!/usr/bin/env python3
import requests
from database import *


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

_base_url = 'https://pro-api.coinmarketcap.com'
headers = {'X-CMC_PRO_API_KEY': '932e9896-d43d-4f5e-87e1-ac4d47ba8df6'}

# Get market data from CMC
log.info("Getting tickers from CoinMarketCap...")

def get_markets():
    # Get market data from CMC
    log.info("Getting markets from CoinMarketCap...")
    url = _base_url + '/v1/cryptocurrency/listings/latest?start=1&limit=500'
    resp = requests.get(url, headers=headers)
    if resp.status_code == 200:
        markets = resp.json()['data']
        return markets
    else:
        return []

def add_curs():
    markets = get_markets()

    for market in markets:
        symbol = market['symbol']
        # Catch and replace inconsistencies between coinmarketcap and ccxt
        if symbol == 'MIOTA':
            symbol = 'IOTA'
        log.debug(f"Checking: {symbol}")
        cur = Currency.query.filter_by(symbol=symbol).first()

        if not cur:
            try: 
                log.debug(f"Adding asset: {symbol}")
                asset = Currency(
                    symbol=symbol,
                    name=market['name'],
                    market_cap=market['quote']['USD']['market_cap'],
                    cmc_id=market['slug']
                    )
                db_session.add(asset)
            except:
                log.debug(f"Couldn't add: {symbol}")

    db_session.commit()


if __name__ == '__main__':
    add_curs()