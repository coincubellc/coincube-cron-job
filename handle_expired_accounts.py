import logging
from database import db_session, Cube

log = logging.getLogger(__name__)



def downgrade_expired():
    cubes = Cube.query.all()

    for cube in cubes:
        # Change Centaur to Index for Cubes which have downgraded
        if cube.user.is_basic:
            if cube.algorithm_id == 8: # Centaur
                cube.algorithm_id = 4
                db_session.add(cube)
        # Change Centaur/Index to Tracker for expired basic/pro accounts
        if not cube.user.is_basic and not cube.user.is_pro:
            cube.algorithm_id = 7 # Tracker
            db_session.add(cube)

    db_session.commit()


