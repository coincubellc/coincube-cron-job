FROM continuumio/miniconda3
RUN apt-get update && apt-get install -y build-essential mysql-client redis-tools default-libmysqlclient-dev

# Python packages
RUN conda install -c conda-forge passlib flask-login flask-wtf flask-mail flask-user celery
RUN pip install mysqlclient
RUN pip install flask-bcrypt

COPY requirements.txt /coincube-cron-job/requirements.txt
RUN pip install -r /coincube-cron-job/requirements.txt

COPY . /coincube-cron-job
WORKDIR /coincube-cron-job

ENTRYPOINT ["/opt/conda/bin/python"]