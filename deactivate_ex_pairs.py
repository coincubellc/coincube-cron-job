from database import *

def deactivate_ex_pairs():
    ex_pairs = ExPair.query.filter_by(active=True).all()

    for ex_pair in ex_pairs:
        if ex_pair.quote_currency.symbol in ['USDT', 'USD', 'USDC','TUSD', 'EUR']:
            if ex_pair.base_currency.symbol != 'BTC':
                ex_pair.active = False
                db_session.add(ex_pair)

    db_session.commit()

def deactivate_index_pairs():
    index_pairs = IndexPair.query.filter_by(active=True).all()

    for index_pair in index_pairs:
        if index_pair.quote_symbol in ['USDT', 'USD', 'USDC','TUSD', 'EUR']:
            if index_pair.base_symbol != 'BTC':
                index_pair.active = False
                db_session.add(index_pair)

    db_session.commit()


if __name__ == '__main__':
    deactivate_ex_pairs()
    deactivate_index_pairs()