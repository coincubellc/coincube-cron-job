<h1>cron job</h1>
This repo contains the Coincube Cron Job Module.

<h2>Getting Started</h2>
In order to get up and running with this repo:

<h2>Submodules</h2>
This repo contains two submodules which need to be pulled down before you will be able to work.<br>

<h3>In order to pull in the submodule `database` you will need to run:</h3>
`git submodule init`<br>
`git submodule update`<br>

<h3>Please be sure to push submodule changes to remote</h3>
<h5 href="https://git-scm.com/book/en/v2/Git-Tools-Submodules">Git Submodule Tutorial #1</h5>
<h5 href="https://git.wiki.kernel.org/index.php/GitSubmoduleTutorial">Git Submodule Tutorial #2</h5>

<h2>Docker Setup</h2>
You will need <a href="https://docker.com" target="_blank">Docker</a>.

Build the Docker container(s):
For local environment: `docker-compose build`<br>

Run the Docker container(s):
For local environment: `docker-compose up`<br>

<h3>Shell into a specific container</h3>
Find "CONTAINER ID": `docker ps`<br>

Shell into container: `docker exec -it "CONTAINER ID" bash`<br>
(i.e. `docker exec -it 78e539ca25be bash`)

<h3>View container logs</h3>
`docker logs -f "CONTAINER ID"`

<h2>Build and push images</h2>
- run docker login `$(aws ecr get-login --no-include-email)`
- build the images with the name of the repo e.g `docker build -t 648212798771.dkr.ecr.us-east-1.amazonaws.com/data:latest .`
- push the images `docker push 648212798771.dkr.ecr.us-east-1.amazonaws.com/data:latest`
Let me know if you need help with something (edited)
