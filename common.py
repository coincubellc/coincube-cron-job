'''
Coin Cube
Common v2
'''
import signal
import threading
import socket
import sys
import logging.handlers
from slacker import Slacker
import gzip
import pandas as pd
from database import *
from time import time, sleep

# set precision to 8 decimal places for logging
pd.options.display.float_format = '{:.8f}'.format
# disable row truncating for logging
pd.options.display.max_rows = None
pd.options.display.max_columns = None


SLACK_API_TOKEN = 'xoxp-3282693918-3282693920-143564691477-7ec7c2e407bbb17423943fea24114b73'
SLACK_CHANNEL = 'data'

SMTPHOST = 'smtp.gmail.com'
SMTPUSER = 'admin@coincube.io'
SMTPPASS = 'Coincube1!'
SMTPTO = ['robert@coincube.io']

DEBUG_LOG_DIR = 'logs'
DEBUG_LOG_COUNT = 30

READ_TABLE_CHUNKSIZE = 2000

# global variables
host = socket.gethostname()
production = host
flushables = []

slack = Slacker(SLACK_API_TOKEN)

def post_message(msg):
    log.info(msg)
    try:
        slack.chat.post_message(SLACK_CHANNEL, msg)
    except:
        log.exception("Slack exception:")

def sigterm_handler(signum, frame):
    # treat SIGTERM as SIGINT (KeyboardInterrupt)
    raise KeyboardInterrupt

def trunc(v, d):
    # truncates v to d decimal places
    try:
        s = '.%df' % (d + 1)
        s = '%%%s' % s
        s = (s % v).split('.')
        t = float('%s.%s' % (s[0], s[1][:d]))
        return t
    except:
        # no decimal place...
        return v

class TimedHandler(logging.Handler):
    '''
    A handler class which buffers FORMATTED logging records in memory,
    periodically flushing them (JOINED BY '\r\n') to a target METHOD.
    Flushing occurs whenever the interval has elapsed, when an event of
    a certain severity or greater is seen, or when manually flushed.
    This class implements an internal thread which handles flushing.
    '''
    def __init__(self, interval, target, flushLevel=logging.ERROR):
        '''
        Initialize the handler with the buffer interval (in seconds),
        the level at which flushing should occur, and a target method.
        '''
        super(TimedHandler, self).__init__()
        self.interval = interval
        self.target = target
        self.flushLevel = flushLevel
        self.buffer = []
        self._stop = threading.Event()
        self._flush = threading.Event()
        self._buffer = threading.Lock()
        self._thread = threading.Thread(target=self._monitor)
        self._thread.daemon = True
        self._thread.start()

    def emit(self, record):
        '''
        Emit a record.
        
        The record is formatted, and then added to the buffer.
        If flushLevel is met, flush the buffer.
        '''
        s = self.format(record)
        with self._buffer:
            self.buffer.append(s)
        if record.levelno >= self.flushLevel:
            self.flush()

    def flush(self):
        '''
        For a TimedHandler, flushing is handled in a separate thread
        once the flush event is set.
        '''
        self._flush.set()
        self._flush.clear()

    def _monitor(self):
        '''
        Monitor for flush events, and send the buffer to the target method.
        
        This method runs on a separate, internal thread.
        The thread will terminate if the handler is stopped.
        '''
        while not self._stop.isSet():
            self._flush.wait(self.interval)
            if self.buffer:
                with self._buffer:
                    msg = '\r\n'.join(self.buffer)
                    self.buffer = []
                try:
                    self.target(msg)
                except:
                    self.handleError(None)

    def close(self):
        '''
        Set the stop event, flush, and wait for the thread to finish.
        '''
        self._stop.set()
        self.flush()
        self._thread.join()
        super(TimedHandler, self).close()

# flush TimedHandlers
def flush():
    for h in flushables:
        h.flush()

# gzip functions for FileHandler
def gzip_namer(name):
    return name + ".gz"

def gzip_rotator(source, dest):
    with open(source, "rb") as sf:
        with gzip.open(dest, "wb") as df:
            df.writelines(sf)
    os.remove(source)

def set_logger(name='__main__',
               slacklevel=logging.WARNING, slackinterval=60, slackchannel=None,
               emaillevel=logging.ERROR,
               sysloglevel=logging.INFO,
               logfile=False, warnings=True):
    global flushables
    module = os.path.splitext(os.path.basename(sys.argv[0]))[0]
    l = logging.getLogger(name)
    l.setLevel(logging.DEBUG)
    # warnings
    if warnings:
        logging.captureWarnings(True)
    wl = logging.getLogger('py.warnings')
    # console
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    cf = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s',
                           datefmt='%H:%M:%S')
    ch.setFormatter(cf)
    l.addHandler(ch)
    wl.addHandler(ch)
    # syslog
    if sysloglevel:
        if 'linux' in sys.platform:
            syslog = '/dev/log'
        elif 'darwin' in sys.platform:
            syslog = '/var/run/syslog'
        else:
            syslog = None
        sh = logging.handlers.SysLogHandler(address=syslog)
        sh.setLevel(sysloglevel)
        sf = logging.Formatter(module +
                               '[%(process)d]: %(levelname)s: %(message)s')
        sh.setFormatter(sf)
        l.addHandler(sh)
        wl.addHandler(sh)
    # email
    if emaillevel:
        eh = logging.handlers.SMTPHandler(SMTPHOST, SMTPUSER, SMTPTO,
            '%s error' % module, credentials=(SMTPUSER, SMTPPASS), secure=())
        eh.setLevel(emaillevel)
        ef = logging.Formatter('%(asctime)s ' + host + ' ' + module
                                + '[%(process)d]: %(levelname)s: %(message)s',
                                datefmt='%b %e %H:%M:%S')
        eh.setFormatter(ef)
        l.addHandler(eh)
        wl.addHandler(eh)
    # slack
    if slacklevel:
        channel = (slackchannel if slackchannel
                   else 'monitoring' if production
                   else host)

        sh = TimedHandler(slackinterval, post_message)
        sh.setLevel(slacklevel)
        sf = logging.Formatter(module + ': %(levelname)s: %(message)s')
        sh.setFormatter(sf)
        l.addHandler(sh)
        wl.addHandler(sh)
        flushables.append(sh)
    # debug file
    if logfile:
        path = os.path.dirname(os.path.abspath(sys.argv[0]))
        l = logging.getLogger()
        l.setLevel(logging.DEBUG)
        fh = logging.handlers.TimedRotatingFileHandler('%s/%s/%s.log' % (path,
            DEBUG_LOG_DIR, module), when='midnight',
            backupCount=DEBUG_LOG_COUNT)
        fh.namer = gzip_namer
        fh.rotator = gzip_rotator
        ff = logging.Formatter('[%(asctime)s] ' + host + ' ' + module
            + '[%(process)d]: %(levelname)s: %(name)s: %(message)s',
            datefmt='%H:%M:%S')
        fh.setFormatter(ff)
        l.addHandler(fh)
    return l

def start(cls, args):
    # interim thread-starter (as copied from rawcollector)
    # takes a list of tuples of arguments for each thread to be started
    try:
        set_logger()
        t = []
        for a in args:
            c = cls(*a)
            t.append(c)
            c.start()
        while True:
            sleep(60)
    except KeyboardInterrupt:
        for c in t:
            c.stop()
        for c in t:
            c.join()
    finally:
        flush()

# treat SIGTERM as SIGINT (KeyboardInterrupt)
signal.signal(signal.SIGTERM, sigterm_handler)
# set precision to 8 decimal places for logging
pd.options.display.float_format = '{:.8f}'.format
# disable row truncating for logging
pd.options.display.max_rows = None
pd.options.display.max_columns = None


def read_df(table, pair, drop_id=True) -> pd.DataFrame:
    """ Loads the data from the table for the given Index/Exchange pair.
    """
    id_attr = 'ex_pair_id' if hasattr(table, 'ex_pair_id') else 'index_pair_id'
    query = table.query.filter(getattr(table, id_attr) == pair.id)
    s = str(query.statement)
    s = s.replace(':' + id_attr + '_1', str(pair.id))

    print('Start query', time())
    gen = pd.read_sql(s, query.session.bind, index_col='timestamp',
                      chunksize=READ_TABLE_CHUNKSIZE)
    print('End query', time())
    chunks = list(gen)
    if not len(chunks):
        return None

    df = pd.concat(chunks)

    if drop_id:
        df.drop([id_attr], axis=1, inplace=True)

    if not len(df):
        return None

    df = df[~df.index.duplicated(keep='first')]
    df.sort_index(inplace=True)
    return df