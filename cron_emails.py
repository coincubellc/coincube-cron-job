import os
from datetime import datetime, timedelta
import logging
import requests as rq
from database import Connection, Invoice, User, UserProduct


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

_email_api_url = os.getenv('EMAIL_URL')


def payment_due_emails():
    ups = UserProduct.query.all()
    for up in ups:
        # One day left to pay
        now = datetime.utcnow()
        if up.paid_until:
            if now + timedelta(days=1) > up.paid_until > now:
                send_payment_due(up.user)
            elif now + timedelta(days=8) > up.paid_until > now + timedelta(days=7):
                send_payment_due(up.user)


def send_payment_due(user):

    user_product = UserProduct.query.filter_by(user_id=user.id).first()
    invoice = Invoice.query.filter_by(
                    user_product_id=user_product.id
                    ).order_by(
                    Invoice.id.desc()).first()

    data = {
        'subject': 'Payment Almost Due',
        'images': 'string',
        'user_id': user.id,
        'username': user.first_name,
        'email': user.email,
        'paid_until': int(user_product.paid_until.timestamp()),
        'monthly_rate': invoice.product_price.monthly_rate,
        'months': invoice.product_price.months,

    }

    url = _email_api_url + '/user_notification/payment_due'
    r = rq.post(url, data=data)
    if r.status_code == 200:
        log.debug(f'{user}: Payment due email sent.')
    else:
        log.debug(r.status_code)


def failed_api_emails():
    connections = Connection.query.filter(
                                Connection.failed_at != None
                                ).all()
    for conn in connections:
        user = User.query.filter_by(id=conn.user_id).first()
        now = datetime.now()
        # Failed within the last hour
        if now > conn.failed_at > now - timedelta(hours=1):
            send_failed_api(user, conn)
        # Failed within the last 3 days
        if now - timedelta(days=3) > conn.failed_at > now - timedelta(days=3, hours=1):
            send_failed_api(user, conn)           


def send_failed_api(user, conn):
    subject = f'Your {conn.exchange.name} API connection has failed'
    data = {
        'subject': subject,
        'images': '',
        'user_id': user.id,
        'username': user.first_name,
        'email': user.email,
        'exchange_name': conn.exchange.name,
        'api_response': '',

    }

    url = _email_api_url + '/user_notification/api_failed'
    r = rq.post(url, data=data)
    if r.status_code == 200:
        log.debug(f'{user}: Failed API connection email sent.')
    else:
        log.debug(r.status_code)


if __name__ == '__main__':
    payment_due_emails()
    failed_api_emails()