import os

REDIS_URI = os.getenv('CELERY_BROKER_URL')

broker_url = REDIS_URI