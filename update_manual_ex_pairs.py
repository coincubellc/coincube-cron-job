from database import *


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

def add_manual_ex_pairs():
    log.info('Adding manual ex_pairs...')
    index_pairs = IndexPair.query.filter_by(active=True).all()

    for ip in index_pairs:
        ex_pair = ExPair.query.filter_by(
                            exchange_id=13,
                            active=True,
                            ).filter(
                            ExPair.base_symbol == ip.base_symbol,
                            ExPair.quote_symbol == ip.quote_symbol,
                            ).first()
        if not ex_pair:
            ex_pair = ExPair(
                        exchange_id=13,
                        quote_currency_id=ip.quote_currency_id,
                        base_currency_id=ip.base_currency_id,
                        quote_symbol=ip.quote_symbol,
                        base_symbol=ip.base_symbol,
                        active=True,
                        )
            log.info(f'Creating ex_pair for index_pair: {ip}')
            ex_pair.save_to_db()

def deactivate_manual_ex_pairs():
    log.info('Removing manual ex_pairs...')
    
    ex_pairs = ExPair.query.filter_by(exchange=13, active=True).all()

    for ep in ex_pairs:
        index_pair = IndexPair.query.filter_by(
                        quote_currency_id=ep.quote_currency_id,
                        base_currency_id=ep.base_currency_id,
                        active=True
                        ).first()
        if not index_pair:
            log.info(f'Deactivating ex_pair: {ep} because matching IndexPair is not active.')
            ep.active = False
            ep.save_to_db()
        else:
            log.info(f'ex_pair: {ep} has a matching active IndexPair.')

def update_manual_eps():
    add_manual_ex_pairs()
    deactivate_manual_ex_pairs()


if __name__ == '__main__':
    update_manual_eps()