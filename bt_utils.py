import logging
import time
from datetime import datetime
from typing import List, Optional

import pandas as pd
import numpy as np
from sqlalchemy import and_, tuple_

from database import IndexPair, CoinMarketCap, CombinedIndexPair1h, db_session, Currency

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)



def get_all_currencies():
    currencies = Currency.query.all()
    return currencies


def load_mcap_data(symbols, start_date, end_date):
    """ Load market capitalization data from MarketCoinCap.
    """
    # TODO: use Currency instead of symbol? (update Gitlab issue)

    # build the query statement
    filter_clause = CoinMarketCap.symbol.in_(symbols)
    if start_date is not None:
        filter_clause = and_(filter_clause, CoinMarketCap.timestamp >= start_date)

    if end_date is not None:
        filter_clause = and_(filter_clause, CoinMarketCap.timestamp <= end_date)
    query = CoinMarketCap.query.filter(filter_clause)

    df = pd.read_sql(query.statement, query.session.bind)

    # get the data from the database
    data = {}
    for sym in df.symbol.unique():
        selection = df[df.symbol == sym]
        ser = (selection.set_index('timestamp')
                .sort_index()
                .market_cap
                .dropna())

        # temporary solution for duplicated rows in CMC table
        ser = ser[~ser.index.duplicated(keep='last')]
        if not len(ser):
            log.debug(f"No non-null MCap data for symbol {sym}. Skipping")
            continue

        # fill any potential gaps in the data
        complete_idx = pd.date_range(ser.index[0], ser.index[-1])
        ser = (ser.reindex(complete_idx)
                  .ffill())

        data[sym] = ser

    return data

def load_OHLC_data(currencies: List[Currency], start_date: datetime = None,
                   end_date: datetime = None, try_cache=True):
    log.info("Requesting data.")
    result = {}

    # use hourly values, so that we can calculate
    # daily OHLC with higher precision in case of non-USD quote
    btc_cur = Currency.query.filter(Currency.symbol == 'BTC').first()
    btc_usd = load_currency(btc_cur, start_date, end_date, None, resample=False)

    for c in currencies:
        log.info("Loading data for " + c.symbol)
        t = time.time()
        df = load_currency(c, start_date, end_date, btc_usd,
                           try_cache=try_cache)

        if df is None or not len(df):
            log.error("Failed to load data for " + c.symbol)
        else:
            log.info(f"{len(df)} bars, {df.index[0]} - {df.index[-1]}. Loaded data in {time.time() - t :.1f}.")

        result[c.symbol] = df

    log.info("Received data.")
    return result


def find_index_pair(currency: Currency, allow_usd_quote=True) -> IndexPair:
    index_pairs = IndexPair.query.filter(IndexPair.base_currency_id == currency.id).all()
    usq = [i for i in index_pairs if i.quote_symbol == 'USD']

    if len(usq) and allow_usd_quote:  # USD quoted pair
        ip = usq[0]
    else:
        btcq = [i for i in index_pairs if i.quote_symbol == 'BTC']
        if not len(btcq):
            log.error(f"Could not find a pair with either USD or BTC quote currency for {currency.symbol}")
            return None

        ip = btcq[0]

    return ip


def load_currency(currency: Currency, start_date: datetime, end_date: datetime,
                  btc_usd: pd.DataFrame, resample=True,
                  use_usd=True, try_cache=False) -> Optional[pd.DataFrame]:

    assert btc_usd is None or (btc_usd.index[-1] - btc_usd.index[-2]).seconds < 3700

    df = None
    index_pair = None
    # if try_cache:
    #     import data_cache
    #     if data_cache.USE_CACHE:
    #         data = data_cache.load_from_cache(currency.id)
    #         if data is not None:
    #             log.info("Loaded data from cache.")
    #             df = data

    if df is None:
        index_pair = find_index_pair(currency, use_usd)

        if index_pair is None:
            return None

        query = index_pair.candle_1h_query
        df = pd.read_sql(query.statement, query.session.bind, index_col='timestamp')

    if df is None or not len(df):
        return None

    requote = (btc_usd is not None and (index_pair is None
               or index_pair.quote_symbol == 'BTC')
               and is_hourly(df))

    df = adjust_df(df, btc_usd, requote, resample, True)
    return df


def is_hourly(df):
    if len(df.index) > 1:
        return (df.index[-1] - df.index[-2]).total_seconds() < 3700
    else:
        return False


def adjust_df(df, btc_usd, requote, resample, ffill=True):
    if requote:
        for column in ['open', 'high', 'low', 'close']:
            df.loc[:, column] = df[column] * btc_usd[column]

    if resample:
        df = df.resample('1D').agg(resample_rules)

    df = df[['open', 'high', 'low', 'close', 'volume']]
    df = df.dropna()
    if len(df) <= 1:
        return None

    if ffill and not is_hourly(df):
        df = forward_fill(df, fill_volume=False, freq='1D')

    return df


resample_rules = dict(open='first', high='max', low='min',
                      close='last', volume='sum')


def load_from_disk():
    with open('data.pkl', 'rb') as f:
        log.info("Using locally stored data.")
        import pickle
        data = pickle.load(f)

    return data


# Code copied from data.candle_merge. Alternatively,
# the data repository can be introduced as a dependency
def forward_fill(candles, fill_volume=False, freq='H'):
    """ Fills missing candles using the last close value. Optionally forward fills volume.

    Should be called after drop_error_candles. If present, `candle_number`
    column values will be set to zero.
    """
    # make sure that no nulls are introduced / all columns are explicitly handled
    for c in candles.columns:
        if c not in ('timestamp', 'open', 'high', 'low', 'close',
                     'volume', 'candle_number'):
            raise Exception(f"`candle_merge.forward_fill` can't handle column `{c}`")

    full_index = pd.date_range(candles.index[0], candles.index[-1], freq=freq)
    candles = candles.reindex(full_index)

    candles.close = candles.close.ffill()
    candles.open = candles.open.fillna(candles.close)
    candles.high = candles.high.fillna(candles.close)
    candles.low = candles.low.fillna(candles.close)
    if fill_volume:
        candles.volume = candles.volume.ffill()
    else:
        candles.volume = candles.volume.fillna(0)

    if 'candle_number' in candles.columns:
        candles.candle_number = candles.candle_number.fillna(0)

    return candles
