from daily_performance import (performance_cubes,
                               performance_account)
from test_accounts import clear_test_accounts
from celery_app import app
from celery.schedules import crontab
from add_currencies import add_curs
from update_close import update_ex_pair_close, update_index_pair_close
from update_ex_pairs import update_eps
from update_ex_pair_limits import update_ex_pair_limits
from update_manual_ex_pairs import update_manual_eps
from handle_expired_accounts import downgrade_expired
from run_indices_backtest import run_daily_backtest
from cron_emails import payment_due_emails, failed_api_emails
from check_pairs import check
from update_transactions import update_all_transactions
from candle_collector_coinapi import collect_candles_coinapi
from candle_collector_crypto_compare import collect_candles_cc
from candle_merge import candle_merge
from market_cap_collector import run_market_cap_collector
from index_candle_generator import generate_index
from database import db_session


class SqlAlchemyTask(app.Task):
    """An abstract Celery Task that ensures that the connection the the
    database is closed on task completion"""
    abstract = True

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        db_session.close()
        db_session.remove()   


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):

    sender.add_periodic_task(
        crontab(minute=30),
        update_close_values,
        name='update all ex_pair close values')

    sender.add_periodic_task(
        crontab(hour=3, minute=1),
        add_missing_curs,
        name='add missing currencies'
        )

    sender.add_periodic_task(
        crontab(hour=4, minute=1),
        check_all_pairs,
        name='check pairs'
        )

    sender.add_periodic_task(
        crontab(hour=5, minute=1),
        run_failed_api_emails,
        name='run failed api emails')

    sender.add_periodic_task(
        crontab(hour=10, minute=1),
        update_all_ex_pairs,
        name='update ex pairs')

    sender.add_periodic_task(
        crontab(hour=11, minute=1),
        run_payment_due_emails,
        name='run payment due emails')

    sender.add_periodic_task(
        crontab(hour=12, minute=1),
        clear_test,
        name='clear test accounts')

    sender.add_periodic_task(
        crontab(hour=22, minute=1),
        downgrade_expired_accounts,
        name='downgrade expired accounts')

    sender.add_periodic_task(
        crontab(hour=23, minute=1),
        run_backtest,
        name='run backtests')

    sender.add_periodic_task(
        crontab(minute=0,
                hour=[1, 3, 5, 7, 9, 11, 13, 15, 17, 21, 23]),
        update_cc_candles,
        name='update cc candles')

    sender.add_periodic_task(
        crontab(minute=20,
                hour=[2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22]),
        merge_candles,
        name='merge candles')

    sender.add_periodic_task(
        crontab(minute=0,
                hour=[4, 8, 12, 16, 22]),
        update_candles_coinapi,
        name='update candles coinapi')

    sender.add_periodic_task(
        crontab(minute=40),
        update_market_cap,
        name='update market cap')

    sender.add_periodic_task(
        crontab(minute=15),
        update_index,
        name='update index')


@app.task(base=SqlAlchemyTask)
def update_cc_candles():
    collect_candles_cc()


@app.task(base=SqlAlchemyTask)
def update_ex_candles():
    collect_candles_ex()


@app.task(base=SqlAlchemyTask)
def update_candles_coinapi():
    collect_candles_coinapi()


@app.task(base=SqlAlchemyTask)
def merge_candles():
    candle_merge()


@app.task(base=SqlAlchemyTask)
def update_market_cap():
    run_market_cap_collector()


@app.task(base=SqlAlchemyTask)
def update_index():
    generate_index()


@app.task(base=SqlAlchemyTask)
def update_performance():
    performance_cubes(timeframe='daily')

@app.task(base=SqlAlchemyTask)
def update_performance_acc():
    performance_account(timeframe='daily')

@app.task(base=SqlAlchemyTask)
def clear_test():
    clear_test_accounts()

@app.task(base=SqlAlchemyTask)
def check_all_pairs():
    check()

@app.task(base=SqlAlchemyTask)
def update_user_transactions():
    update_all_transactions()

@app.task(base=SqlAlchemyTask)
def add_missing_curs():
    add_curs()

@app.task(base=SqlAlchemyTask)
def run_payment_due_emails():
    payment_due_emails()

@app.task(base=SqlAlchemyTask)
def run_failed_api_emails():
    failed_api_emails()

@app.task(base=SqlAlchemyTask)
def update_close_values():
    update_ex_pair_close()
    update_index_pair_close()

@app.task(base=SqlAlchemyTask)
def update_all_ex_pairs():
    update_eps()
    update_manual_eps()
    update_ex_pair_limits()

@app.task(base=SqlAlchemyTask)
def downgrade_expired_accounts():
    downgrade_expired()

@app.task(base=SqlAlchemyTask)
def run_backtest():
    run_daily_backtest()

@app.task(base=SqlAlchemyTask)
def update_index():
    generate_index()