import unittest

import pandas as pd

from backtest_rebalance import rebalance_EW, rebalance_MW, apply_rebal_threshold, mcap_selector


class BacktestTests(unittest.TestCase):
    def test_rebalance_EW(self):
        mon = 100.0
        symbol_list = ['A', 'B', 'C']
        prices = dict(A=1, B=1, C=3)

        p = rebalance_EW(None, mon, prices, symbol_list)

        self.assertAlmostEqual(p['A'], 33.333333, places=4)
        self.assertAlmostEqual(p['B'], 33.333333, places=4)
        self.assertAlmostEqual(p['C'], 11.111111, places=4)

    def test_rebalance_MW(self):
        dt = 0
        mon = 100
        symbol_list = ['A', 'B', 'C']
        prices = dict(A=1, B=1, C=2)

        mcap = dict(A=50, B=30, C=20)
        mcap = {sym: pd.Series([m], [0]) for sym, m in mcap.items()}

        p = rebalance_MW(dt, mon, prices, symbol_list, mcap)
        self.assertAlmostEqual(p['A'], 50, places=4)
        self.assertAlmostEqual(p['B'], 30, places=4)
        self.assertAlmostEqual(p['C'], 10, places=4)

    def test_apply_rebal_threshold(self):
        threshold = 0.05
        cur_positions = dict(A=10, B=20, C=10)
        new_positions = dict(A=10, B=20.5, C=20)

        res = apply_rebal_threshold(cur_positions, new_positions, threshold)
        self.assertAlmostEqual(res['A'], 10, places=4)
        self.assertAlmostEqual(res['B'], 20, places=4)
        self.assertAlmostEqual(res['C'], 20, places=4)
        self.assertEqual(len(res), 3)

    def test_mcap_selector(self):
        symbols = ['A', 'B', 'C', 'D', 'E']
        date = 0

        mcap = dict(A=50, B=30, C=20, D=50, E=25)
        mcap = {sym: pd.Series([m], [0]) for sym, m in mcap.items()}

        data_bounds = dict(A=(1, 1), B=(0, 1), C=(0, 1),
                           D=(0, 1), E=(0, 1))

        selection = mcap_selector(symbols, date, mcap, index_size=3, data_bounds=data_bounds)

        self.assertEqual(selection, ['D', 'B', 'E'])

