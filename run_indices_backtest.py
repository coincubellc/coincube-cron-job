import logging
from datetime import datetime, timedelta
from functools import partial
from time import time

from backtest_rebalance import mcap_selector, backtest, RebalanceType, InsufficientDataException
from bt_utils import get_all_currencies, load_OHLC_data, load_mcap_data
from database import BacktestResultsSecondary, engine, db_session

#########################################

FIATS = ['USD', 'EUR', 'GBP', 'CAD']  # fiat currencies, to skip

# index sizes
TOP_NS = [
    (5, 'five'),
    (10, 'ten'),
    (20, 'twenty'),
    (30, 'thirty'),
    (50, 'fifty'),
]

# index rebalancing types, one for each index size
REBAL_TYPES = [
    (RebalanceType.EqualyWeighted, 'ew'),
    (RebalanceType.MCapWeighted, 'mcw'),
]

rebal_threshold = 0.05  # None to disable

start_date = datetime(2017, 1, 1)
end_date = datetime.utcnow()

start_capital = 1000

save_to_database = True

# whether to build the index or throw an exception if
# the number of currencies available for a date is
# smaller than the index size
allow_adaptive_index_construction = True

skip_symbols = []

#########################################


log = logging.getLogger()
log.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
cf = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s',
                       datefmt='%H:%M:%S')
ch.setFormatter(cf)
log.addHandler(ch)


def backtest_indices(start_date, end_date, **kwargs):
    """ Runs backtest on all indices specified in the config section above.
    """
    if not save_to_database:
        log.warning("!!!! Saving to database is disabled !!!!")

    try:
        log.info("Starting backtest for indices.")
        currencies = get_all_currencies()
        currencies = [s for s in currencies if s.symbol not in FIATS + skip_symbols]
        data = load_OHLC_data(currencies, start_date, end_date)

        universe = [c.symbol for c in currencies]

        log.info("Loading data.")
        t = time()
        log.info(f"Finished loading data in {time() - t :.2f}.")
        mcap_data = load_mcap_data(universe, start_date, end_date)

        results = {}

        # prepare the secondary table
        if save_to_database:
            if not BacktestResultsSecondary.__table__.exists(engine):
                BacktestResultsSecondary.__table__.create(engine)
                db_session.commit()

            BacktestResultsSecondary.query.delete()
            db_session.commit()
    except:
        log.exception("Unhandled exception:")

    # loop through index, rebal combs
    for rebal_type, suffix in REBAL_TYPES:
        for top, name in TOP_NS:
            try:
                selector = partial(mcap_selector, index_size=top)
                name = f'top_{name}_{suffix}'

                log.info(f"Starting backtest for index '{name}'")

                result = backtest(universe, start_date, end_date, start_capital,
                                  backtest_name=name + "_btest", data=data,
                                  mcap_data=mcap_data, save_to_db=save_to_database,
                                  index_name=name, symbol_selector=selector,
                                  rebalance_type=rebal_type, rebalance_threshold=rebal_threshold,
                                  adaptive_index_construction=allow_adaptive_index_construction,
                                  **kwargs)

                results[name] = result
            except InsufficientDataException as e:
                log.error(str(e) + f". Index name: '{name}'")
            except:
                log.exception("Unhandled exception:")

    # the backtest results were saved to the secondary table, so
    # now that the backtest is done, we can switch tables
    if save_to_database:
        try:
            log.info("Backtest complete. Switching secondary 'backtest_results' with primary.")
            if engine.dialect.has_table(engine, "backtest_results_backup"):
                log.debug("Removing table backtest_results_backup.")
                db_session.execute("DROP TABLE backtest_results_backup")
                db_session.commit()

            if engine.dialect.has_table(engine, "backtest_results"):
                log.debug("Renaming backtest_results to backtest_results_backup.")
                db_session.execute("RENAME TABLE backtest_results TO backtest_results_backup")
                db_session.commit()

            log.debug("Renaming backtest_results_secondary to backtest_results.")
            db_session.execute("RENAME TABLE backtest_results_secondary TO backtest_results")
            db_session.commit()
        except:
            log.exception("Unhandled exception:")

    log.info("Finished backtest for indices.")
    return results


def run_daily_backtest():
    """ Runs backtests for all indices. Should be run once per day.
    """
    # import data_cache
    # data_cache.USE_CACHE = False
    start_date = datetime(2017, 1, 1)
    end_date = datetime.utcnow().replace(hour=0, minute=0, second=0)
    end_date -= timedelta(days=1)
    backtest_indices(start_date, end_date)


if __name__ == '__main__':
    results = run_daily_backtest()
